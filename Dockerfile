From ubuntu:20.04

MAINTAINER alucky4416 <alucky4416@yahoo.co.jp>

RUN apt-get update && apt-get install -y \
    python3 \
    cmake \
    ninja-build

RUN cd /tmp; mkdir sdkdir;
COPY oecore-x86_64-core2-64-toolchain-6.0.sh /tmp/sdkdir
RUN chmod +x /tmp/sdkdir/oecore-x86_64-core2-64-toolchain-6.0.sh \
    && /tmp/sdkdir/oecore-x86_64-core2-64-toolchain-6.0.sh
RUN rm -rf /tmp/sdkdir
